﻿# The script of the game goes in this file.

# Declare characters used by this game. The color argument colorizes the
# name of the character.

define e = Character("Eileen")
define s = Character('Sylvie', color="#c8ffc8")
define m = Character('Me', color="#c8c8ff")
define joel = Character("Joel")
define hunt = Character("Hunter")
define phil = Character("Phillip")
define ant = Character("Antonio")
define hawk = Character("Hawke")
define koi = Character("Koichi")

default book = False
default happyEnding = False
# The game starts here.

label start:

    # Show a background. This uses a placeholder by default, but you can
    # add a file (named either "bg room.png" or "bg room.jpg") to the
    # images directory to show it.

    scene bg school
    "You're a young woman almost finished with university and you're looking for an internship at (such & such occupation of choice)." 
    "Your favorite hobbies include hanging at your favorite cafe, doing new interesting things, and of course, finding \"the one\"--with a long term relationship in mind... And who knows... Someday you'll be married!"
label introductions:
    "Choose a man to learn about"
menu:
    "Joel":
        jump joelSantos
    "Hunter":
        jump hunterDragos
    "Phil":
        jump phillipSmith
    "???":
        jump antonio
    "Hawke":
        jump hawkeTruson
    "Koichi":
        jump koichiEbihara
    "finished":
        jump begin

label begin:
    "What's a visual novel?"

menu:
    "It's a videogame.":
        jump game
    "It's an interactive book.":
        jump book

label game:

    m "It's a kind of videogame you can play on your computer or a console."

    jump marry

label book:
    $ book = True
    m "It's like an interactive book that you can read on a computer or a console."
    jump marry

label marry:
    if book:
        "You hit the flag book"
        jump goodEnd
    else:
        "You missed the flag"
        jump badEnd

label joelSantos:
    show redboy at center
    "Your best friend who you've known since freshman in highschool who supports every decision you make."
    "You know he's in love with you (he confessed senior year of high school), but you (at the same time), didn't feel the same."
    "After his confession, you became best friends with him; he knows you inside out, your quirks, good and bad."
    "He's also surprisingly popular with the ladies on campus; thankfully(?) he only has eyes got you."
    "He is an all-rounder kinf of guy, average athletics, decent smarts, and a friendly guy that everyone likes being around, thus his popularity. Age:23"
    hide redboy
    jump introductions

label hunterDragos:
    show boy at center
    "\"The Dragon\" Your new boss where you're interning. He's not much older than you, but is strict and good at his job."
    "Not to mention he's handsome and single. He knows you have potential, so he's very touch on you and likes to pick on you because you're new."
    "Kind of like an older brother...If a bit twisted because he secretly crushes on you. Many of the older office ladies often glare at you in jealousy due to attention he gives you. Age:27"
    hide boy
    jump introductions

label phillipSmith:
    show greenboy at center
    "Your co-worker at your part-time job while hunting for an internship and sometimes classmate. You've seen hum around, but he's kind of shy."
    "He's extremely knowledgeable about (choice of occupation) and helps you out when you get stuck or confused. Age: 25"
    hide greenboy
    jump introductions

label antonio:
    show starboy at center
    "He's someone you randomly met at your favorite cafe. You continously run into him here, and you find out that this cafe is also where he loves to hang out."
    "He's always reading an interesting book about something or other. You know his favorite types of books are science-fiction, fantasy, & adventure."
    "He's otherwise pretty mysterious. Hell, you don't even know his name!! \"Antonio\" sounds like a name he made up when you first ask hom after he notices you. Age: ??"
    hide starboy
    jump introductions

label hawkeTruson:
    show blueboy at center
    "Hunter's best friend who is kind and gentle, if a bit distant." 
    "The total opposite of Hunter, he's always hanging around the office to help with odd jobs and errands (there's a rumor that he's actually the CEO of the company, but...?)"
    "He can also be found at your university library! But why's he there? Age: 28"
    hide blueboy
    jump introductions

label koichiEbihara:
    show spinboy at center
    "A somewhat older businessman who appears set with life and is obviously living well."
    "He does business with the company you're interning at and you catch his eyes because..."
    "You quite literally bump into him. He seems hateful and frigid but is actually very lonely. Can you open his heart? Age: 30"
    hide spinboy
    jump introductions
label badEnd: 
    "And so, we become a visual novel creating duo."
    ".:. Meh Ending"
    return
label goodEnd:
    $ happyEnding = True
    "And so we hit it off and it was happily ever after"
    ".:. Good Ending"
    return
label normalEnd:
    return
label trueEnd:
    return
label polyamorousEnd:
    return
label epilogue:
    return
